const { Gitlab } = require('@gitbeaker/node');
const api = new Gitlab({
  host: 'https://gitlab.com/',
  token: process.env.GL_USER_TOKEN,
});

const projectId = process.env.CI_PROJECT_ID;
const commitDescription = process.env.CI_COMMIT_DESCRIPTION;
const mergeRequestId = commitDescription.match(/!\d+/)[0].substring(1);
const commitMsg = process.env.CI_COMMIT_MESSAGE;
const branchName = commitMsg.match(/^Merge branch '(.+)' into/)[1];
const taskName = branchName.match(/CMOHR-\d+/g)[0];
const targetBranch = 'test';
const label = 'to_test';

async function getTargetCommit() {
  const { squash_commit_sha } = await api.MergeRequests.show(projectId, Number(mergeRequestId));
  return squash_commit_sha;
}
async function createBranch() {
  try {
    const newBranchName = `${branchName}_to-test`;
    await api.Branches.create(projectId, newBranchName, targetBranch);

    // cherry-pick
    const commitToCherryPickSha = await getTargetCommit();
    await api.Commits.cherryPick(projectId, commitToCherryPickSha, newBranchName);

    return newBranchName;
  } catch (error) {
    console.error(`Error creating new branch: ${error.message}`);
    throw error;
  }
}

async function createMergeRequest(sourceBranch) {
  const mrTitle = `Cherry pick ${taskName} to ${targetBranch}`;
  const mrDescription = `:exclamation: Этот MR создан автоматически! В нем не стоит ничего менять :exclamation: \ Cherry pick ${taskName} to ${targetBranch}`;
  return await api.MergeRequests.create(projectId, sourceBranch, targetBranch, mrTitle, {
    description: mrDescription,
    autoMergeStrategy: 'merge_when_pipeline_succeeds',
  });
}

async function main() {
  const mergeRequest = await api.MergeRequests.show(projectId, Number(mergeRequestId));
  if (mergeRequest.labels.includes(label)) {
    const branch = await createBranch();
    const newMergeRequest = await createMergeRequest(branch);
    console.log(`New merge request created: ${newMergeRequest.web_url}`);
  } else {
    console.log('No action required.');
  }
}

main()
  .then(() => console.log('Done'))
  .catch((error) => console.error(error));
